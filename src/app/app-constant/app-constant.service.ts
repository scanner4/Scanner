import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AppConstantService {

  constructor( public toastCtrl: ToastController,) { }

    // Toster
    async presentToast(msg): Promise<any> {
      const toast = await this.toastCtrl.create({
        message: msg,
        duration: 4000,
        color: 'danger',
      });
      toast.present();
    }
  
    async presentToastForLogin(msg) {
      const toast = await this.toastCtrl.create({
        message: msg,
        duration: 4000,
        color: 'success',
      });
      toast.present();
    }
}
