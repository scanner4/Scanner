import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public scannedData: any = {};
  encodedData: '';
  encodeData: any;
  inputData: any;
  scanArray: any = [];
  qrData = null;
  createdCode = null;
  constructor(private barcodeScanner: BarcodeScanner, public router: Router, private social: SocialSharing) {
    // Check if sharing via email is supported
    this.social.canShareViaEmail().then(() => {
      // Sharing via email is possible
      console.log("23");

    }).catch(() => {
      // Sharing via email is not possible
    });
  }

  ionViewWillEnter() {
    this.scanArray = JSON.parse(localStorage.getItem('scan_history'));
    if (!localStorage.getItem('loginInfo')) {
      this.router.navigate(['login']);
    }
    console.log(this.scanArray);

  }

  scanNow() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      saveHistory: true,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417 ',
      orientation: 'portrait',
    };

    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', JSON.stringify(barcodeData));
      this.scannedData = barcodeData;
      this.scanArray = this.scanArray || [];
      this.scanArray.push(barcodeData)
      localStorage.setItem("scan_history", JSON.stringify(this.scanArray));
      console.log(this.scanArray);
    }).catch(err => {
      console.log('Error', err);
    });

    this.scanArray.forEach(element => {
      if(element.cancelled == true){
        this.router.navigate(['home']);
      }
    });

  }

  email() {
    // Share via email
    this.social.shareViaEmail(this.createdCode, 'Subject', ['test@test.com']).then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }

  ShareWhatsapp() {
    this.social.shareViaWhatsApp(this.createdCode, '', '');
  }

  createCode() {
    this.createdCode = this.qrData;
    console.log(this.createdCode);
  }

}
