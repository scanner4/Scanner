import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppConstantService } from '../app-constant/app-constant.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  profileForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(public constant: AppConstantService, public router: Router) { }

  ngOnInit() { }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
    if (this.profileForm.value) {
      if (this.profileForm.value.email == 'test@test.com' && this.profileForm.value.password == '8256455') {
        this.constant.presentToastForLogin("Login Successfully");
        localStorage.setItem('loginInfo', JSON.stringify(this.profileForm.value));
        setTimeout(() => {
          this.router.navigate(['home']);
        }, 1000);
      } else {
        this.constant.presentToast("Please Enter Valid Credentials");
      }
    }
  }

}
